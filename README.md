Advisory of Avast Ticket-ID AWG-552-201-9:
- Avast AntiVirus (aswSnx.sys) IOCTL 0x82AC0204 Out-of-Bound Read
- No CVE is assigned

Timeline:
- 2019-08-21	Reported vulnerability and POC to Avast
- 2019-09-05	Avast acknowledged report and assigned ticket-ID AWG-552-201-9
- 2019-09-25	Avast replied that this vulnerability will be fixed in v19.9